#!/bin/sh

chmod 644 /opt/etc/afp.conf

systemctl enable netatalk
systemctl start netatalk

systemctl enable netatalk-cnid_metad
systemctl start netatalk-cnid_metad

mkdir -p /storage/modroot/etc /storage/modroot/.work
systemctl enable etc.mount
systemctl start etc.mount

systemctl restart avahi-daemon




# libreelec_time_machine

Time Machine configuration for LibreELEC.

Basically, a copy-paste of great instructions at https://www.artembutusov.com/libreelec-as-time-machine-backup-network-device/

# Basic things to remember
Files are relative to $HOME (which currently is /storage) and .config has been renamed here to dot.config.

Execute this for the Time Machine to be visible before the first use:

`defaults write com.apple.systempreferences TMShowUnsupportedNetworkVolumes 1`


